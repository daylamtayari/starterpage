# Starter page

This is a browser starter page who's intended use is to be used as a browser home page.

## Installation:

 1. Download all files.
 2. Input your latitude, longitude and your Open Weather App ID (free and required to utilize the  Open Weather API). 
 3. Change the background image if you wish.
 4. Set your home page to the `home.html`file in your browser settings.
 5. Enjoy!

![Starterpage Image](https://imgur.com/kp8APu7.jpg)

## Credits:

 - The [Startpages subreddit](https://old.reddit.com/r/startpages/) for all of the inspiration.
 - Jared 'Jaredk3nt' Jones for the inspiration and who's homepage was a major inspiration and the foundation of this project. https://github.com/Jaredk3nt
 - Mark Thompson (Getty Images) for the photograph who's edited version is the background of this start page. 
 - Open Weather for the free to use weather API.
